package com.company;

import java.util.Locale;

public class Bank {
    public Bank(float rateUSDtoEUR, float rateUSDtoRU, float rateEURtoRU, String name) {
        this.rateUSDtoEUR = rateUSDtoEUR;
        this.rateUSDtoRU = rateUSDtoRU;
        this.rateEURtoRU = rateEURtoRU;
        this.name = name;
    }

    float rateUSDtoEUR;
    float rateUSDtoRU;
    float rateEURtoRU;
    String name;

    String currencyExchange(String name, float value) {
        if (value * rateEURtoRU > Float.MAX_VALUE) {
            return new String("Where have you taken so much money?");
        }
        if (name.equalsIgnoreCase("USD")) {
            return new StringBuilder()
                    .append("in Euro ")
                    .append(String.format(Locale.US, "%.2f", value * rateUSDtoEUR))
                    .append(" in Rubles ")
                    .append(String.format(Locale.US, "%.2f", value * rateUSDtoRU)).toString();
        }
        if (name.equalsIgnoreCase("EUR")) {
            return new StringBuilder()
                    .append("in USD ")
                    .append(String.format(Locale.US, "%.2f", value / rateUSDtoEUR))
                    .append(" in Rubles ")
                    .append(String.format(Locale.US, "%.2f", value * rateEURtoRU)).toString();
        }
        if (name.equalsIgnoreCase("RU")) {
            return new StringBuilder()
                    .append("in USD ")
                    .append(String.format(Locale.US, "%.2f", value / rateUSDtoRU))
                    .append(" in Euro ")
                    .append(String.format(Locale.US, "%.2f", value / rateEURtoRU)).toString();
        }
        return null;
    }
}
