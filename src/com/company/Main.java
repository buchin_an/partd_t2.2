package com.company;

import java.util.Scanner;

public class Main {

    static final int ID_PRIVATE_BANK = 0;
    static final int ID_PUMB = 1;
    static final int ID_OSHADBANK = 2;


    static String[] banksOfUkraine = {"PrivateBank", "PUMB", "Oshadbank"};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the amount of money that you want to change: ");
        float amount = Float.parseFloat(scanner.nextLine());

        System.out.println("Enter your currency (USD or EUR or RU): ");
        String currency = scanner.nextLine();

        System.out.println("Choose your bank (PrivateBank or PUMB or Oshadbank): ");
        String bankName = scanner.nextLine();


        if (bankName.equalsIgnoreCase(banksOfUkraine[ID_PRIVATE_BANK])) {
            Bank privateBank = new Bank(0.81f, 57.56f, 0.81f, "PrivateBank");
            System.out.println(privateBank.currencyExchange(currency, amount));

        }
        if (bankName.equalsIgnoreCase(banksOfUkraine[ID_PUMB])) {
            Bank pumb = new Bank(0.79f, 55.55f, 0.79f, "Pumb");
            System.out.println(pumb.currencyExchange(currency, amount));

        }
        if (bankName.equalsIgnoreCase(banksOfUkraine[ID_OSHADBANK])) {
            Bank oshadbank = new Bank(0.75f, 51.88f, 0.74f, "Oshadbank");
            System.out.println(oshadbank.currencyExchange(currency, amount));

        }
    }
}
